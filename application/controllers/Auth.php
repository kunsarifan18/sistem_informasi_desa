<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('AuthModel', 'auth');
	}

	public function index()
	{
		$this->load->view('auth');
	}

	function process_login()
	{
		$user_email = $this->input->post('user_name', TRUE);
        $user_password = sha1($this->input->post('user_password', TRUE));
        
        $validate = $this->auth->validate($user_email,$user_password);
        if($validate->num_rows() > 0){
            $data  = $validate->row_array();
            $username  = $data['user_name'];
            $email = $data['user_email'];
            $level = $data['role'];
            $sesdata = array(
                'name'  => $name,
                'user_email'     => $email,
                'role'     => $level,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sesdata);
            // access login for admin
            if($level === '1'){
                redirect('admin');
            }else{
                redirect('supir');
            }
        }else{
            // echo $this->session->set_flashdata('msg','Username or Password is Wrong');
            $this->session->set_flashdata('message', 'swal("Gagal!", "Email / Password yang anda masukan Salah!!", "warning");');
            redirect('auth');
        }
	}
}
