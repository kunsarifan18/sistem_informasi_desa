<?php
class Template{
    protected $_ci;
    
    function __construct(){
        $this->_ci = &get_instance();
    }
    
  function home($content, $data = NULL){
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
        $data['head'] = $this->_ci->load->view('layout/head', $data, TRUE);
        $data['navbar'] = $this->_ci->load->view('layout/navbar', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footer'] = $this->_ci->load->view('layout/footer', $data, TRUE);
        
        $this->_ci->load->view('layout/index', $data);
    }

  function layout($content, $data = NULL){
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
        $data['head'] = $this->_ci->load->view('admin/templates/head', $data, TRUE);
        $data['sidebar'] = $this->_ci->load->view('admin/templates/sidebar', $data, TRUE);
        $data['content'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footer'] = $this->_ci->load->view('admin/templates/footer', $data, TRUE);
        
        $this->_ci->load->view('admin/templates/index', $data);
    }
}