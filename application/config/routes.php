<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['about'] = 'guest/about';

$route['default_controller'] = 'guest';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
