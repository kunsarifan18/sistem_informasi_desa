<nav class="navbar navbar-expand-lg navbar-dark bg-warning shadow">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Logo</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= site_url('')?>">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= site_url('about')?>">About Us</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown-menu" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Pelayanan
          </a>
          <ul class="dropdown-menu" id="myDropdown">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      <li class="nav-item">
          <a class="nav-link" href="<?= site_url('auth')?>">Login</a>
        </li>
      </ul>
    </div>
  </div>
</nav>