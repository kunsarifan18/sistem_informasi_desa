<!DOCTYPE html>
<html lang="en">
<?= $head ?>
<body>
<?= $navbar ?>
<?= $content ?>

</body>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="<?= base_url('assets');?>/js/jquery.min.js"></script>
<!-- <script type="text/javascript" src="<?= base_url('assets');?>/js/bootstrap.bundle.js"></script> -->
<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {
  var myDropdown = document.getElementById("myDropdown");
    if (myDropdown.classList.contains('show')) {
      myDropdown.classList.remove('show');
    }
  }
}
</script>
</html>