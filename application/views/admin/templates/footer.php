<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">Apakah anda yakin ingin keluar!!
                
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url('assets')?>/sb-admin/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url('assets')?>/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url('assets')?>/sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url('assets')?>/sb-admin/js/sb-admin-2.min.js"></script>
     <!-- Page level plugins -->
     <script src="<?= base_url('assets')?>/sb-admin/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets')?>/sb-admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>