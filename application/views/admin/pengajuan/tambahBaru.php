<section class="content">
	<div class="container mt-2">
		<div class="card bg-secondary text-center">
			<div class="card-header bg-light">Pengajuan KTP Baru</div>
			<div class="card-body text-white">
				<form action="">
					<div class="mb-3 row">
						<label class="col-sm-4 col-form-label">NIK*</label>
						<div class="col-sm-7">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="mb-3 row">
						<label class="col-sm-4 col-form-label">Nomor Induk KK*</label>
						<div class="col-sm-7">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="mb-3 row">
						<label for="inputPassword" class="col-sm-4 col-form-label">Nama *</label>
						<div class="col-sm-7">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="mb-3 row">
						<label for="inputPassword" class="col-sm-4 col-form-label">Tempat *</label>
						<div class="col-sm-7">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="mb-3 row">
						<label class="col-sm-4 col-form-label">Tanggal Lahir *</label>
						<div class="col-sm-7">
							<input type="date" class="form-control">
						</div>
					</div>
                    <button type="submit" class="btn btn-sm btn-success">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</section>
