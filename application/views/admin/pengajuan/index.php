 <div class="container-fluid">
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Data Pengajuan KTP Baru</h1>

  <div class="card shadow mb-4">
  	<div class="card-body">
  		<div class="table-responsive">
  			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
  				<thead class="thead-dark">
  					<tr>
  						<th>Name</th>
  						<th>Position</th>
  						<th>Office</th>
  						<th>Age</th>
  						<th>Start date</th>
  						<th>Salary</th>
  					</tr>
  				</thead>
  				<tbody>
  					<tr>
  						<td>Donna Snider</td>
  						<td>Customer Support</td>
  						<td>New York</td>
  						<td>27</td>
  						<td>2011/01/25</td>
  						<td>$112,000</td>
  					</tr>
  				</tbody>
  			</table>
  		</div>
  	</div>
  </div>
 </div>
