<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login System</title>
	<link rel="stylesheet" href="<?= base_url('assets');?>/css/bootstrap.min.css">

</head>

<body class="bg-secondary">
	<div class="container">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-5 login">
					<div class="card-title bg-dark text-white text-center">
						<h5>
							Login!!
						</h5>
					</div>
					<div class="bg-blue">
						<form action="<?= base_url('auth/proccess_login')?>">
							<div class="form-group">
								<label>Email</label>
								<input type="email" class="form-control">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input type="password" class="form-control">
							</div>
							<button type="submit" class="btn btn-primary float-right">Login</button>
							<br>
							<p>Belum Mempunyai Akun Daftar <a href="">disini</a></p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
